"""
Написати функцію, яка приймає 2 аргументи – цілі числа. Всередині функції виконується
перевірка типу. Якщо хоча б одне з них не int, то повертається 1, якщо обидва int, то
рахується їхня сума. Якщо сума додатня, повертається 0, якщо від’ємна, то -1.
"""


def tricky_check(num_1: int, num_2: int) -> int:
    if all([isinstance(num_1, int), isinstance(num_2, int)]):
        if num_1 + num_2 > 0:
            return 0
        elif num_1 + num_2 < 0:
            return -1
    else:
        return 1


if __name__ == "__main__":
    print(tricky_check(2,3))